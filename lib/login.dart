import 'package:flutter/material.dart';
import 'package:mobile_small_enterprise_sales/components/common_theme.dart';
import 'package:mobile_small_enterprise_sales/components/form/button.dart';
import 'package:mobile_small_enterprise_sales/components/form/password_field.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  // login variable
  String username, password;

  String _passwordValidor(String value) {
    if (value == null || value == "") {
      return "Password is Empty";
    } else {
      setState(() {
        password = value;
      });
    }
    return null;
  }

  String _usernameValidator(String value) {
    if ((value == null || value == "")) {
      return "Username is Empty";
    } else {
      setState(() {
        username = value;
      });
    }
    return null;
  }

  void _onLoginButton() {
    if (_formKey.currentState.validate()) {
      openDialog(context, Text('Valid'));
    }
  }

  void openDialog(BuildContext context, Widget content) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AlertDialog(
            content: content,
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          ),
    );
  }

  OutlineInputBorder _inputBorderDecoration() {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
        borderSide: BorderSide(color: Colors.indigo));
  }

  void _onRegisterButton() {
    openDialog(context, Text('Click Register Button'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
          child: Center(
            child: SingleChildScrollView(
              child: Form(
                key: _formKey,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  child: Column(
                    children: <Widget>[
                      Image.asset("assets/images/mses-logo.png"),
                      SizedBox(
                        height: 30.0,
                      ),
                      TextFormField(
                        validator: _usernameValidator,
                        onSaved: (value) => setState(() {
                              username = value;
                            }),
                        decoration: InputDecoration(
                            enabledBorder: _inputBorderDecoration(),
                            hintStyle: TextStyle(color: Colors.indigo[100]),
                            hintText: "Please input your username",
                            border: _inputBorderDecoration(),
                            prefixIcon: Icon(Icons.account_circle)),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      PasswordField(
                        hintText: "Please input your password",
                        border: _inputBorderDecoration(),
                        prefixIcon: Icon(Icons.lock),
                        validator: _passwordValidor,
                        onSaved: (value) => setState(() {
                              password = value;
                            }),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: CustomButton(
                              paramBorderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  bottomLeft: Radius.circular(10)),
                              color: Colors.white,
                              textColor: Colors.indigo,
                              onPressed: _onRegisterButton,
                              text: "Register",
                            ),
                          ),
                          Expanded(
                            child: LoginButton(
                              paramBorderRadius: BorderRadius.only(
                                  topRight: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                              onPressed: _onLoginButton,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
