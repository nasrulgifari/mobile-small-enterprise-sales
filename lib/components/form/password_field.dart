import 'package:flutter/material.dart';

class PasswordField extends StatefulWidget {
  final String value;
  final Widget prefixIcon;
  final ValueChanged<String> onChange;
  final FormFieldValidator<String> validator;
  final FormFieldSetter<String> onSaved;
  final TextEditingController controller;
  final InputBorder border;
  final String hintText;
  final String labelText;

  const PasswordField(
      {Key key,
      this.value,
      this.prefixIcon,
      this.onChange,
      this.validator,
      this.onSaved,
      this.controller,
      this.border,
      this.hintText,
      this.labelText})
      : super(key: key);

  @override
  _PasswordFieldState createState() => _PasswordFieldState();
}

class _PasswordFieldState extends State<PasswordField> {
  String passwordValue;
  bool isVisible = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextFormField(
          controller: this.widget.controller,
          validator: this.widget.validator,
          onSaved: this.widget.onSaved,
          obscureText: _isObscure(),
          decoration: InputDecoration(
              enabledBorder: this.widget.border,
              hintStyle: TextStyle(color: Colors.indigo[100]),
              labelText: this.widget.labelText,
              hintText: this.widget.hintText,
              border: this.widget.border,
              prefixIcon: this.widget.prefixIcon,
              suffixIcon: IconButton(
                icon: Icon(_checkVisibility()),
                onPressed: _changeVisibility,
              ))),
    );
  }

  bool _isObscure() {
    return !isVisible;
  }

  IconData _checkVisibility() {
    return (isVisible == true) ? Icons.visibility_off : Icons.visibility;
  }

  void _changeVisibility() {
    setState(() {
      isVisible = !isVisible;
    });
  }
}
