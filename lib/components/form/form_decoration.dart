import 'package:flutter/material.dart';

class LogonFormDecoration extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return OutlineInputBorder(
        borderRadius: BorderRadius.circular(10.0),
        borderSide: BorderSide(color: Colors.blue));
  }
}
