import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  final VoidCallback onPressed;
  final BorderRadiusGeometry borderRadius;

  LoginButton({Key key, this.onPressed, BorderRadiusGeometry paramBorderRadius})
      : borderRadius = (paramBorderRadius == null)
            ? BorderRadius.circular(10.0)
            : paramBorderRadius,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: this.borderRadius),
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        textColor: Colors.white,
        color: Colors.indigo,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: Text(
            'Login',
            style: TextStyle(fontSize: 20.0),
          ),
        ),
        onPressed: this.onPressed);
  }
}

class CustomButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String text;
  final Color textColor;
  final Color color;
  final BorderRadiusGeometry borderRadius;

  CustomButton(
      {Key key,
      this.onPressed,
      this.text,
      this.textColor,
      this.color,
      BorderRadiusGeometry paramBorderRadius})
      : borderRadius = (paramBorderRadius == null)
            ? BorderRadius.circular(10.0)
            : paramBorderRadius,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
        shape: RoundedRectangleBorder(
            side: BorderSide(color: this.textColor),
            borderRadius: this.borderRadius),
        padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
        textColor: this.textColor,
        color: this.color,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: Text(
            this.text,
            style: TextStyle(fontSize: 20.0),
          ),
        ),
        onPressed: this.onPressed);
  }
}
