import 'dart:async';

import 'package:flutter/material.dart';
import 'package:mobile_small_enterprise_sales/login.dart';

class InitialScreen extends StatefulWidget {
  @override
  _InitialScreenState createState() => _InitialScreenState();
}

class _InitialScreenState extends State<InitialScreen> {
  final _scaffoldKey = GlobalKey<ScaffoldState> ();
  Timer initialScreenTimer;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
        body: Center(
      child: Container(
        decoration: BoxDecoration(color: Theme.of(context).canvasColor),
        child: Image.asset("assets/images/mses-logo.png"),
      ),
    ));
  }

  void _goToLogin()
  {
    Navigator.of(_scaffoldKey.currentContext).pushReplacement(MaterialPageRoute(builder: (context) => Login()));
  }

@override
  void initState() {
    // TODO: implement initState
    super.initState();
    initialScreenTimer = Timer(Duration(seconds: 3), _goToLogin);
  }
  
}
